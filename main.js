'use strict'
const fileUploader = document.getElementById('file-uploader');
const reader = new FileReader();
const deleteButton = document.getElementById('delete-button');

fileUploader.addEventListener('change', (event) => {
    const files = event.target.files;  
    const contain = files[0];
    let objJSON = {};
    reader.readAsText(contain, 'UTF-8');
    reader.onload = function() {
        objJSON = JSON.parse(reader.result);
        let section = document.querySelector('.section'); 
        let showWrap = document.createElement('div');
        showWrap.classList.add('wrap');
        section.append(showWrap);

        showHeader (objJSON);
        showInput (objJSON);
        showRef (objJSON);
        showButton (objJSON);

        // Add common header
        function showHeader (obj) {      
            let showH2 = document.createElement('h2');
            showH2.textContent = obj['name'];
            showH2.classList.add('name');  
            showWrap.append(showH2);  
        }

        // Add inputs
        function showInput (obj) {
            let inputsArr = obj['fields'];
            for (let i = 0; i < inputsArr.length; i++) {
                let inputWrap = document.createElement('div');
                let input = document.createElement('input');
                if (inputsArr[i].label == undefined || inputsArr[i].label.includes(' ')) {
                    inputWrap.classList.add(inputsArr[i].input.type)
                } else {
                    inputWrap.classList.add(inputsArr[i].label);  
                }              
                showWrap.append(inputWrap);

                let label = document.createElement('label');
                if (inputsArr[i].label) {
                    label.setAttribute('for', inputsArr[i].label);
                    label.textContent = inputsArr[i].label;
                    inputWrap.append(label);
                }   

                if (inputsArr[i].input.required == true) {
                    input.setAttribute('required', '');
                }  
                if (inputsArr[i].input.multiple == true) {
                    input.setAttribute('multiple', '');
                }
                if (inputsArr[i].input.filetype != undefined) {
                    let formats = inputsArr[i].input.filetype.join(', .');
                    input.setAttribute('accept', '.' + formats);
                }
                if (inputsArr[i].input.placeholder) {
                    input.setAttribute('placeholder', inputsArr[i].input.placeholder);
                }
                if (inputsArr[i].input.type == 'technology') {
                    input.setAttribute('list', 'select');
                    let datalist = document.createElement('datalist');
                    datalist.id = 'select';
                    datalist.setAttribute('multiple', '');
                    inputWrap.append(datalist);
                    for (let value of inputsArr[i].input.technologies) {
                        let option = document.createElement('option');
                        option.textContent = value;
                        datalist.append(option);
                    }                    
                }
                if (inputsArr[i].input.mask != undefined) {
                    $(function(){
                        $(input).mask(inputsArr[i].input.mask);
                      });
                }
                
                if (inputsArr[i].input.type !== 'textarea' && inputsArr[i].input.type !== 'number') {
                    input.setAttribute('type', inputsArr[i].input.type);
                    input.id = inputsArr[i].label;
                    if (inputsArr[i].input.type == 'text' && inputsArr[i].label) {
                        input.setAttribute('placeholder', inputsArr[i].label);
                    }
                    inputWrap.append(input);
                } else if (inputsArr[i].input.type == 'number') {
                    input.setAttribute('type', 'text');
                    inputWrap.append(input);
                } else {
                    let textarea = document.createElement('textarea');
                    textarea.id = inputsArr[i].label;
                    textarea.setAttribute('placeholder', inputsArr[i].label);
                    inputWrap.append(textarea);
                }       

                if (inputsArr[i].input.type == 'color') {
                    input.setAttribute('list', 'select');
                    let datalist = document.createElement('datalist');
                    datalist.id = 'select';
                    inputWrap.append(datalist);
                    for (let value of inputsArr[i].input.colors) {
                        let option = document.createElement('option');
                        option.textContent = value;
                        datalist.append(option);
                    }
                }
            }
        }

        // Add refs
        function showRef (obj) {
            let refArr = obj['references'];
            let refWrap = document.createElement('div');
            refWrap.classList.add('ref');
            showWrap.append(refWrap); 
            for (let i = 0; i < refArr.length; i++) {
                if (refArr[i].input) {
                    let input = document.createElement('input');
                    input.classList.add(refArr[i].input.type);
                    input.setAttribute('type', refArr[i].input.type);
                    if (refArr[i].input.required) {
                        input.setAttribute('required', true);
                    }
                    if (refArr[i].input.checked == 'true') {
                        input.setAttribute('checked');
                    }
                    refWrap.append(input);
                } else if (refArr[i]['text without ref']) {
                    let parag = document.createElement('p');
                    parag.classList.add('ref-p');
                    parag.textContent = refArr[i]['text without ref'];
                    refWrap.append(parag);

                    let ref = document.createElement('a');
                    ref.classList.add('ref-a');
                    ref.textContent = refArr[i]['text'];
                    ref.setAttribute('href', refArr[i]['ref']);           
                    refWrap.append(ref);   
                }            
            }
        }

        // Add button
        function showButton (obj) {
            let buttonArr = obj['buttons'];
            for (let i = 0; i < buttonArr.length; i++) {
                let button = document.createElement('button');
                button.classList.add('button');
                button.textContent = buttonArr[i]['text'];
                showWrap.append(button);
                
            }
        }
       
    };   
    reader.onloadend = function() {
        const elem = document.querySelectorAll('.wrap');
        deleteButton.onclick = function () {
            for (let item of elem) {
                 item.remove();
            }           
        }
    }
}); 
    
   
   
